#include <cstring>
#include <algorithm>
#include <cstdlib>
#include "phone_call.h"
#include <iostream>
#include <vector>

using namespace std;

static int (*criterion_fuction)(phone_call* left, phone_call* right);

bool heap_compare(phone_call* left, phone_call* right)
{
	return criterion_fuction(left, right) < 0;
}

void heap_sort(phone_call* array[], int size, int (*criterion)(phone_call* left, phone_call* right))
{
	criterion_fuction = criterion;
	vector<phone_call*> v(array, array + size);
	make_heap(v.begin(), v.end(), heap_compare);
	sort_heap(v.begin(), v.end(), heap_compare);
	for (int i = 0; i < size; i++) array[i] = v[i];
}

int quick_compare(const void* left, const void* right)
{
	return criterion_fuction(*(phone_call**)left, *(phone_call**)right);

}

void quick_sort(phone_call* array[], int size, int (*criterion)(phone_call* left, phone_call* right)) 
{
	criterion_fuction = criterion;
	qsort(array, size, sizeof(phone_call*), quick_compare);
}

int sort_by_duration(phone_call* left, phone_call* right) 
{
	int left_duration = left->duration.second;
	left_duration += left->duration.minute * 60;
	left_duration += left->duration.hour * 3600;
	int right_duration = right->duration.second;
	right_duration += right->duration.minute * 60;
	right_duration += right->duration.hour * 3600;
	return right_duration - left_duration;
}

double get_cost(phone_call* call) 
{
	double minutes = call->duration.minute;
	minutes += call->duration.hour * 60.0;
	minutes += call->duration.second / 60.0;
	return minutes * call->rate;
}

int sort_by_number_then_cost(phone_call* left, phone_call* right) 
{
	int result = strcmp(left->number, right->number);
	if (result == 0) {
		return (int) (get_cost(right) - get_cost(left));
	}
	else return result;
}

