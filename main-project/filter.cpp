#include "filter.h"
#include <cstring>
#include <iostream>

phone_call** filter(phone_call* array[], int size, bool (*check)(phone_call* element), int& result_size) 
{
	phone_call** result = new phone_call * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_mobile_call(phone_call* element) 
{
	return strcmp(element->tariff, "mobile") == 0;
}

bool check_call_by_date(phone_call* element)
{
	return element->date.month == 11 && element->date.year == 2021;
}