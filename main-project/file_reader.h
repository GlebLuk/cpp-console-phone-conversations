#ifndef FILE_READER_H
#define FILE_READER_H

#include "phone_call.h"

void read(const char* file_name, phone_call* array[], int& size);

#endif
