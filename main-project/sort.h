#ifndef SORT_H
#define SORT_H

#include "phone_call.h"

void heap_sort(phone_call* array[], int size, int (*criterion)(phone_call* left, phone_call* right));

void quick_sort(phone_call* array[], int size, int (*criterion)(phone_call* left, phone_call* right));

int sort_by_duration(phone_call* left, phone_call* right);

double get_cost(phone_call* call);

int sort_by_number_then_cost(phone_call* left, phone_call* right);

#endif