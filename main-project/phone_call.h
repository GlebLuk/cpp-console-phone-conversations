#ifndef PHONE_CALL_H
#define PHONE_CALL_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};

struct timing
{
    int hour;
    int minute;
    int second;
};

struct phone_call
{
    char number[MAX_PHONE_NUMBER_SIZE];
    date date;
    timing start;
    timing duration;
    char tariff[MAX_TARIFF_LENGTH];
    double rate;
};

#endif
