#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

date convert_to_date(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.year = atoi(str_number);
    return result;
}

timing convert_to_time(char* str)
{
    timing result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.minute = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.second = atoi(str_number);
    return result;
}

void read(const char* file_name, phone_call* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            phone_call* item = new phone_call;
            file >> item->number;
            file >> tmp_buffer;
            item->date = convert_to_date(tmp_buffer);
            file >> tmp_buffer;
            item->start = convert_to_time(tmp_buffer);
            file >> tmp_buffer;
            item->duration = convert_to_time(tmp_buffer);
            file >> item->tariff;
            file >> item->rate;
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}