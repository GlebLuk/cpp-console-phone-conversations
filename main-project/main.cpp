#include <iostream>
#include <iomanip>

using namespace std;

#include "phone_call.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "sort.h"

void output(phone_call* call) 
{
    cout
        << setw(INDENT) << setfill(' ') << "����� ��������: "
        << call->number << '\n'
        << setw(INDENT) << setfill(' ') << "���� ������: "
        << setw(2) << setfill('0') << call->date.day << '.'
        << setw(2) << setfill('0') << call->date.month << '.'
        << call->date.year << '\n'
        << setw(INDENT) << setfill(' ') << "����� ������ ������: "
        << setw(2) << setfill('0') << call->start.hour << ':'
        << setw(2) << setfill('0') << call->start.minute << ':'
        << setw(2) << setfill('0') << call->start.second << '\n'
        << setw(INDENT) << setfill(' ') << "����������������� ���������: "
        << setw(2) << setfill('0') << call->duration.hour << ':'
        << setw(2) << setfill('0') << call->duration.minute << ':'
        << setw(2) << setfill('0') << call->duration.second << '\n'
        << setw(INDENT) << setfill(' ') << "�������� ������: "
        << call->tariff << '\n'
        << setw(INDENT) << setfill(' ') << "��������� ������ ���������: "
        << call->rate << '\n'
        << setw(INDENT) << setfill(' ') << "��������� ����� ���������: "
        << fixed << setprecision(2) << get_cost(call);
    cout << endl << endl;
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �9. ���������� ���������\n";
    cout << "�����: ���� ��������\n";
    cout << "������: 14\n\n";
    phone_call* phone_calls[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", phone_calls, size);
        cout << "           ***** ���������� ��������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(phone_calls[i]);
        }

        bool (*check_function)(phone_call*) = NULL;
        cout << "\n�������� ������ ����������:\n";
        cout << "1) ������� ��� ���������� ��������� �� ��������� ��������\n";
        cout << "2) ������� ��� ���������� ��������� � ������ 2021 ����\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        if (item == 1) check_function = check_mobile_call;
        else if (item == 2) check_function = check_call_by_date;
        else throw "������������ ����� ������";


        void (*sort_method)(phone_call**, int, int (*criterion)(phone_call*, phone_call*)) = NULL;
        cout << "\n�������� ����� ����������:\n";
        cout << "1) ������������� ����������\n";
        cout << "2) ������� ���������� \n";
        cout << "\n������� ����� ���������� ������: ";
        int method_selection;
        cin >> method_selection;
        cout << '\n';
        if (method_selection == 1) sort_method = heap_sort;
        else if (method_selection == 2) sort_method = quick_sort;
        else throw "������������ ����� ������";


        int (*sort_criterion)(phone_call*, phone_call*) = NULL;
        cout << "\n�������� �������� ����������:\n";
        cout << "1) �� �������� ����������������� ���������\n";
        cout << "2) �� ����������� ������ ��������, � � ������ ������ ������ �� �������� ��������� ���������\n";
        cout << "\n������� ����� ���������� ������: ";
        int criterion_selection;
        cin >> criterion_selection;
        cout << '\n';
        if (criterion_selection == 1) sort_criterion = sort_by_duration;
        else if (criterion_selection == 2) sort_criterion = sort_by_number_then_cost;
        else throw "������������ ����� ������";

        if (check_function)
        {
            int new_size;
            phone_call** filtered = filter(phone_calls, size, check_function, new_size);
            sort_method(filtered, new_size, sort_criterion);
            if (item == 1) cout << "***** ��� ���������� ��������� �� ��������� ��������\n";
            else if (item == 2) cout << "***** ��� ���������� ��������� � ������ 2021 ����\n";
            if (method_selection == 1) cout << "��������������� ������� ������������� ����������\n";
            else if (method_selection == 2) cout << "��������������� ������� ������� ����������\n";
            if (criterion_selection == 1) cout << "�� �������� ����������������� ��������� ***** \n\n";
            else if (criterion_selection == 2) cout << "�� ����������� ������ ��������, � � ������ ������ ������ �� �������� ��������� ��������� ***** \n\n";
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete phone_calls[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
